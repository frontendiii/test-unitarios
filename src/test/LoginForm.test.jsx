import { fireEvent, render, screen } from "@testing-library/react";
import { describe, expect, test, vi } from "vitest";
import LoginForm from "../Components/LoginForm";


describe('Login Form testing', () => {
    test('Should render title', () => {
        render(<LoginForm/>)
        const title = screen.getByText(/acceder a recetas dh/i)
        expect(title).toBeDefined()
    })
    test('Should render first input', () => {
        render(<LoginForm/>)
        const email = screen.getByRole('email')
        expect(email).toBeDefined()
    })
    test('Should change second input', () => {
        render(<LoginForm/>)
        const password = screen.getByTestId('password')
        fireEvent.change(password, {target: {value: 'contraseña123'}}) //event.target.value
        expect(password.value).toBe('contraseña123')
    })
    test('Should call onClick function', () => {
        const handleClick = vi.fn()
        render(<LoginForm handleClick={handleClick}/>)
        const button = screen.getByText('Enviar')
        fireEvent.click(button)
        expect(handleClick).toBeCalledTimes(1)
    })
    test('Should not allow numeric values in email input', () => {
        render(<LoginForm/>)
        const email = screen.getByRole('email')
        fireEvent.change(email, {target: {value: '123'}})
        expect(email.value).toBe('123')
    })
    test('Should trigger submit event with non-numeric email and valid password', () => {
        const handleClick = vi.fn()
        render(<LoginForm handleClick={handleClick}/>)
        const email = screen.getByRole('email')
        const password = screen.getByTestId('password')
        const button = screen.getByText('Enviar')
        fireEvent.change(email, {target: {value: 'test@test.com'}})
        fireEvent.change(password, {target: {value: 'contraseña123'}})
        fireEvent.click(button)
        expect(handleClick).toBeCalledTimes(1)
        const submmitData = handleClick.mock.calls[0][0]
        expect(submmitData).toBeDefined()
        expect(submmitData.target).toBeDefined()
        expect(email.value).toBe('test@test.com')
        expect(password.value).toBe('contraseña123')  
    })
})